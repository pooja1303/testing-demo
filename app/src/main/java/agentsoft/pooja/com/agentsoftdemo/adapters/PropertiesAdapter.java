package agentsoft.pooja.com.agentsoftdemo.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import agentsoft.pooja.com.agentsoftdemo.R;
import agentsoft.pooja.com.agentsoftdemo.activities.PropertyDetailsActivity;
import agentsoft.pooja.com.agentsoftdemo.models.PropertiesListModel;

/**
 * Created by POOJA on 11-09-2017.
 */

public class PropertiesAdapter extends RecyclerView.Adapter<PropertiesAdapter.ViewHolder>{
    Context mContext;
    ArrayList<PropertiesListModel> mPropList;

    public PropertiesAdapter(Context mContext, ArrayList<PropertiesListModel> mPropList) {
        this.mContext = mContext;
        this.mPropList = mPropList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_property_list_item, parent, false);
        ViewHolder holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.txt_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, PropertyDetailsActivity.class);
                intent.putExtra("from","property");
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mPropList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView txt_view;
        public ViewHolder(View itemView) {
            super(itemView);

            txt_view=(TextView)itemView.findViewById(R.id.txt_view);
        }
    }
}
