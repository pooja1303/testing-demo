package agentsoft.pooja.com.agentsoftdemo.activities;

import android.animation.Animator;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import agentsoft.pooja.com.agentsoftdemo.fragments.CustomerListFragment;
import agentsoft.pooja.com.agentsoftdemo.fragments.DashboardFragment;
import agentsoft.pooja.com.agentsoftdemo.fragments.PropertyListFragment;
import agentsoft.pooja.com.agentsoftdemo.R;
import agentsoft.pooja.com.agentsoftdemo.fragments.ReportsFragment;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar mToolbar;
    private ImageView addfilter;
    private LinearLayout llFilter;
    private FloatingActionButton fab, fab1, fab2, fab3;
    private LinearLayout fabLayout1, fabLayout2, fabLayout3;
    private View fabBGLayout;
    boolean isFABOpen = false;

    private SharedPreferences mPref;
    private LinearLayout ll_dashboard, ll_property, ll_customer, ll_reports, ll_linBase;
    private ImageView iv_customer, iv_reports, iv_property, iv_dashboard;
    private TextView txt_dashboard, txt_property, txt_customer, txt_reports;
    // private MaterialBetterSpinner spPropertyType,spPropertyFor,spPropertySize;
    private int count = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //  getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
        tx.replace(R.id.content_frame, new DashboardFragment());
        tx.commit();

        getLoginPrefData();
        setAppToolbar();
        initailizeComponents();
        setListeners();
    }

    private void setListeners() {
        ll_customer.setOnClickListener(this);
        ll_dashboard.setOnClickListener(this);
        ll_property.setOnClickListener(this);
        ll_reports.setOnClickListener(this);
       /* addfilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int i = count++;
                if ((i % 2 == 0)) {
                    llFilter.setVisibility(View.VISIBLE);
                }else{
                    llFilter.setVisibility(View.GONE);
                }
            }
        });*/
        /*fabAddNewProp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // add form to fill fields// call new activity

                Intent in=new Intent(MainActivity.this,PropertyRegistration.class);
                startActivity(in);
                finish();
            }
        });*/
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isFABOpen) {
                    showFABMenu();

                    fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_clear));
                } else {
                    closeFABMenu();
                    fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_add));
                }
            }
        });
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(MainActivity.this, PropertyRegistration.class);
                startActivity(in);
                finish();
            }
        });
        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(MainActivity.this, CustomerRegistration.class);
                startActivity(in);
                finish();
            }
        });
        fabBGLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeFABMenu();
                fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_add));
            }
        });
    }

    private void setAppToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
    }

    private void getLoginPrefData() {
        mPref = getSharedPreferences("Login", Context.MODE_PRIVATE);


    }

    private void initailizeComponents() {
        iv_customer = (ImageView) findViewById(R.id.iv_customer);
        iv_reports = (ImageView) findViewById(R.id.iv_reports);
        iv_property = (ImageView) findViewById(R.id.iv_property);
        iv_dashboard = (ImageView) findViewById(R.id.iv_dashboard);

        txt_customer=(TextView)findViewById(R.id.txt_customer);
        txt_dashboard=(TextView)findViewById(R.id.txt_dashboard);
        txt_property=(TextView)findViewById(R.id.txt_property);
        txt_reports=(TextView)findViewById(R.id.txt_reports);

        ll_linBase = (LinearLayout) findViewById(R.id.ll_linBase);

        fabLayout1 = (LinearLayout) findViewById(R.id.fabLayout1);
        fabLayout2 = (LinearLayout) findViewById(R.id.fabLayout2);
        //  fabLayout3= (LinearLayout) findViewById(R.id.fabLayout3);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab1 = (FloatingActionButton) findViewById(R.id.fab1);
        fab2 = (FloatingActionButton) findViewById(R.id.fab2);
        //fab3 = (FloatingActionButton) findViewById(R.id.fab3);
        fabBGLayout = findViewById(R.id.fabBGLayout);

        ll_reports = (LinearLayout) findViewById(R.id.ll_reports);
        ll_dashboard = (LinearLayout) findViewById(R.id.ll_dashboard);
        ll_property = (LinearLayout) findViewById(R.id.ll_property);
        ll_customer = (LinearLayout) findViewById(R.id.ll_customer);

        /*spPropertyType = (MaterialBetterSpinner)findViewById(R.id.sp_prop_type);
        spPropertyFor = (MaterialBetterSpinner)findViewById(R.id.sp_prop_for);
        spPropertySize = (MaterialBetterSpinner)findViewById(R.id.sp_prop_size);
         llFilter = (LinearLayout) findViewById(R.id.ll_filter);
        addfilter=(ImageView)findViewById(R.id.addfilter);*/

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.property_type));
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.property_type));
        ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.property_size));

       /* spPropertyType.setAdapter(adapter);
        spPropertyFor.setAdapter(adapter2);
        spPropertySize.setAdapter(adapter3);*/


        // mChart1.spin(2000, 0, 360);

        /*Legend l = mChart1.getLegend();
         l = mChart2.getLegend();
         l = mChart3.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setEnabled(false);
*/

    }

    private void showFABMenu() {
        isFABOpen = true;
        fabLayout1.setVisibility(View.VISIBLE);
        fabLayout2.setVisibility(View.VISIBLE);
        // fabLayout3.setVisibility(View.VISIBLE);
        fabBGLayout.setVisibility(View.VISIBLE);

        fab.animate().rotationBy(180);
        fabLayout1.animate().translationY(-getResources().getDimension(R.dimen.standard_55));
        fabLayout2.animate().translationY(-getResources().getDimension(R.dimen.standard_100));
        // fabLayout3.animate().translationY(-getResources().getDimension(R.dimen.standard_145));
    }

    private void closeFABMenu() {
        isFABOpen = false;

        fabBGLayout.setVisibility(View.GONE);
        fab.animate().rotationBy(-180);
        fabLayout1.animate().translationY(0);
        //fabLayout2.animate().translationY(0);
        fabLayout2.animate().translationY(0).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                if (!isFABOpen) {
                    fabLayout1.setVisibility(View.GONE);
                    fabLayout2.setVisibility(View.GONE);
                    //fabLayout3.setVisibility(View.GONE);
                }

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        if (isFABOpen) {
            closeFABMenu();
            fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_add));
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.ll_customer:

                setActiveImageColor(iv_customer, iv_dashboard, iv_reports, iv_property);

                setActiveTextColor(txt_customer, txt_dashboard, txt_property, txt_reports);


                FragmentTransaction custFrag = getSupportFragmentManager().beginTransaction();
                custFrag.replace(R.id.content_frame, new CustomerListFragment());
                custFrag.commit();
                break;

            case R.id.ll_dashboard:
               /* iv_dashboard.setColorFilter(getResources().getColor(R.color.colorPrimary));
                iv_customer.setColorFilter(getResources().getColor(R.color.md_grey_500));
                iv_reports.setColorFilter(getResources().getColor(R.color.md_grey_500));
                iv_property.setColorFilter(getResources().getColor(R.color.md_grey_500));*/

                setActiveImageColor(iv_dashboard, iv_customer, iv_reports, iv_property);

                setActiveTextColor(txt_dashboard, txt_customer, txt_property, txt_reports);

                FragmentTransaction dashFrag = getSupportFragmentManager().beginTransaction();
                dashFrag.replace(R.id.content_frame, new DashboardFragment());
                dashFrag.commit();
                break;

            case R.id.ll_reports:
               /* iv_reports.setColorFilter(getResources().getColor(R.color.colorPrimary));
                iv_customer.setColorFilter(getResources().getColor(R.color.md_grey_500));
                iv_dashboard.setColorFilter(getResources().getColor(R.color.md_grey_500));
                iv_dashboard.setColorFilter(getResources().getColor(R.color.md_grey_500));*/

                setActiveImageColor(iv_reports, iv_customer, iv_dashboard, iv_property);

                setActiveTextColor(txt_reports, txt_dashboard, txt_customer, txt_property);

                FragmentTransaction reportsFrag = getSupportFragmentManager().beginTransaction();
                reportsFrag.replace(R.id.content_frame, new ReportsFragment());
                reportsFrag.commit();
                break;

            case R.id.ll_property:
              /*  iv_property.setColorFilter(getResources().getColor(R.color.colorPrimary));
                iv_customer.setColorFilter(getResources().getColor(R.color.md_grey_500));
                iv_dashboard.setColorFilter(getResources().getColor(R.color.md_grey_500));
                iv_reports.setColorFilter(getResources().getColor(R.color.md_grey_500));*/

                setActiveImageColor(iv_property, iv_customer, iv_dashboard, iv_reports);

                setActiveTextColor(txt_property, txt_reports, txt_dashboard, txt_customer);

                FragmentTransaction propFrag = getSupportFragmentManager().beginTransaction();
                propFrag.replace(R.id.content_frame, new PropertyListFragment());
                propFrag.commit();
                break;

        }
    }

    private void setActiveTextColor(TextView activeText, TextView inactiveText1, TextView inactiveText2, TextView inactiveText3) {
        activeText.setTextColor(getResources().getColor(R.color.colorPrimary));
        inactiveText1.setTextColor(getResources().getColor(R.color.md_grey_500));
        inactiveText2.setTextColor(getResources().getColor(R.color.md_grey_500));
        inactiveText3.setTextColor(getResources().getColor(R.color.md_grey_500));
        inactiveText2.setTextColor(getResources().getColor(R.color.md_grey_500));
    }

    private void setActiveImageColor(ImageView activeImg, ImageView inactiveImg1, ImageView inactiveImg2, ImageView inactiveImg3) {

        activeImg.setColorFilter(getResources().getColor(R.color.colorPrimary));
        inactiveImg1.setColorFilter(getResources().getColor(R.color.md_grey_500));
        inactiveImg2.setColorFilter(getResources().getColor(R.color.md_grey_500));
        inactiveImg3.setColorFilter(getResources().getColor(R.color.md_grey_500));


    }

}
