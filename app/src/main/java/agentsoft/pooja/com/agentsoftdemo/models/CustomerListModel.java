package agentsoft.pooja.com.agentsoftdemo.models;

import android.graphics.drawable.Drawable;

/**
 * Created by POOJA on 9/17/2017.
 */

public class CustomerListModel {
    public Drawable cust_img;
    public String cust_name;
    public String contact_no;
    public String property_for;
    public String location;
    public String property_size;
    public String status;
    public String budget;
    public String property_availability;


    public CustomerListModel(Drawable cust_img, String cust_name, String contact_no, String property_for, String location, String property_size, String status, String budget,String property_availability) {
        this.cust_img = cust_img;
        this.cust_name = cust_name;
        this.contact_no = contact_no;
        this.property_for = property_for;
        this.location = location;
        this.property_size = property_size;
        this.status = status;
        this.budget = budget;
        this.property_availability = property_availability;
    }
}
