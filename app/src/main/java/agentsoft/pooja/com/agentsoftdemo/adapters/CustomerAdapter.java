package agentsoft.pooja.com.agentsoftdemo.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.SearchView;
import android.widget.TextView;

import java.util.ArrayList;

import agentsoft.pooja.com.agentsoftdemo.R;
import agentsoft.pooja.com.agentsoftdemo.activities.CustomerRegistration;
import agentsoft.pooja.com.agentsoftdemo.activities.PropertyDetailsActivity;
import agentsoft.pooja.com.agentsoftdemo.models.CustomerListModel;
import agentsoft.pooja.com.agentsoftdemo.models.LeadHistoryModel;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by POOJA on 11-09-2017.
 */

public class CustomerAdapter extends RecyclerView.Adapter<CustomerAdapter.ViewHolder> {
    Context mContext;
    ArrayList<CustomerListModel> mCustList;

    public CustomerAdapter(Context mContext, ArrayList<CustomerListModel> mCustList) {
        this.mContext = mContext;
        this.mCustList = mCustList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_cust_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder,final int position) {

        holder.txt_lead_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openLeadPopUp(position);
            }
        });

        holder.txt_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, PropertyDetailsActivity.class);
                intent.putExtra("from","customer");
                mContext.startActivity(intent);
            }
        });

    }

    private void openLeadPopUp(int position) {
        Log.v("CustomerAdapter ","openLeadPopUp: position:  " +position);
        //  LayoutInflater layoutInflater= (LayoutInflater)mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        //  View popupView = layoutInflater.inflate(R.layout.layout_custom_lead_history_dialog, null);
        //   final PopupWindow popupWindow = new PopupWindow( popupView,LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
        final Dialog dialog = new Dialog(mContext);
        dialog.setContentView(R.layout.layout_custom_lead_history_dialog);
        dialog.setTitle("Lead History");
        SearchView searchView = (SearchView) dialog.findViewById(R.id.simpleSearchView);
        changeSearchViewTextColor(searchView);
        TextView btn_exit=(TextView)dialog.findViewById(R.id. btn_exit);
        RecyclerView mLeadHisRecycler = (RecyclerView) dialog.findViewById(R.id.lead_history_recyclerView);
        mLeadHisRecycler.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mLeadHisRecycler.setLayoutManager(mLayoutManager);


        //  setRecyclerAdapter();
        // addLeadList();
        ArrayList<LeadHistoryModel> mList = new ArrayList<LeadHistoryModel>();
        for(int i=0;i<=4;i++) {
            mList.add(new LeadHistoryModel("Confirmed", "20/02/2017", "", "Shubham", "Remarks"));
        }
        LeadHistoryAdapter mAdapter = new LeadHistoryAdapter(mContext, mList);
        mLeadHisRecycler.setAdapter(mAdapter);
        btn_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        //  popupWindow.showAtLocation(holder.txt_lead_history);

        dialog.show();

    }

    private void changeSearchViewTextColor(View view) {
        if (view != null) {
            if (view instanceof TextView) {
                ((TextView) view).setTextColor(Color.BLACK);
                return;
            } else if (view instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) view;
                for (int i = 0; i < viewGroup.getChildCount(); i++) {
                    changeSearchViewTextColor(viewGroup.getChildAt(i));
                }
            }
        }
    }
    @Override
    public int getItemCount() {
        return mCustList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_lead_history,txt_view;

        public ViewHolder(View itemView) {
            super(itemView);
            txt_lead_history = (TextView) itemView.findViewById(R.id.txt_lead_history);
            txt_view = (TextView) itemView.findViewById(R.id.txt_view);
        }
    }
}
