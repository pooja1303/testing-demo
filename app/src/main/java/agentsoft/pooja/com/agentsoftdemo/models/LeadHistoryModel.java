package agentsoft.pooja.com.agentsoftdemo.models;

/**
 * Created by POOJA on 9/18/2017.
 */

public class LeadHistoryModel {
    public String status;
    public String modified_date;
    public String visited_date;
    public String modified_by;
    public String remarks;

    public LeadHistoryModel(String status, String modified_date, String visited_date, String modified_by, String remarks) {
        this.status = status;
        this.modified_date = modified_date;
        this.visited_date = visited_date;
        this.modified_by = modified_by;
        this.remarks = remarks;
    }
}
