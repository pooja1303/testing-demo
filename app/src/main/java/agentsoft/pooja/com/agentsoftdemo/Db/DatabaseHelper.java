package agentsoft.pooja.com.agentsoftdemo.Db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by POOJA on 9/3/2017.
 */

public class DatabaseHelper extends SQLiteOpenHelper {
    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "AgentSoftDb";

    // table name
    private static final String TABLE_PROPERTY_TYPE = "property_type";
    private static final String TABLE_PROPERTY_AGE = "property_age";
    private static final String TABLE_PROPERTY_REFFERED_BY = "property_reffered_by";
    private static final String TABLE_PROPERTY_ONFLOOR = "property_onfloor";
    private static final String TABLE_PROPERTY_SIZE = "property_size";
    private static final String TABLE_PROPERTY_AVAILABILITY = "property_availability";

    //  Table Columns names
    private static final String ID = "id";
    private static final String VALUE = "value";
    private static final String FLAG = "flag";
    private static final String STATUS = "status";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_PROPERTY_TYPE_TABLE =
                "CREATE TABLE " + TABLE_PROPERTY_TYPE + "("
                        + ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                        + VALUE + " TEXT,"
                        + FLAG + " TEXT,"
                        + STATUS + " TEXT" + ")";

        String CREATE_PROPERTY_AGE_TABLE =
                "CREATE TABLE " + TABLE_PROPERTY_AGE + "("
                        + ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                        + VALUE + " TEXT,"
                        + FLAG + " TEXT,"
                        + STATUS + " TEXT" + ")";

        String CREATE_PROPERTY_REFFERED_BY_TABLE =
                "CREATE TABLE " + TABLE_PROPERTY_REFFERED_BY + "("
                        + ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                        + VALUE + " TEXT,"
                        + FLAG + " TEXT,"
                        + STATUS + " TEXT" + ")";

        String CREATE_PROPERTY_ONFLOOR_TABLE =
                "CREATE TABLE " + TABLE_PROPERTY_ONFLOOR + "("
                        + ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                        + VALUE + " TEXT,"
                        + FLAG + " TEXT,"
                        + STATUS + " TEXT" + ")";

        String CREATE_PROPERTY_SIZE_TABLE =
                "CREATE TABLE " + TABLE_PROPERTY_SIZE + "("
                        + ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                        + VALUE + " TEXT,"
                        + FLAG + " TEXT,"
                        + STATUS + " TEXT" + ")";
        String CREATE_PROPERTY_AVAILABILITY_TABLE =
                "CREATE TABLE " + TABLE_PROPERTY_AVAILABILITY + "("
                        + ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                        + VALUE + " TEXT,"
                        + FLAG + " TEXT,"
                        + STATUS + " TEXT" + ")";

        db.execSQL(CREATE_PROPERTY_TYPE_TABLE);
        db.execSQL(CREATE_PROPERTY_AGE_TABLE);
        db.execSQL(CREATE_PROPERTY_REFFERED_BY_TABLE);
        db.execSQL(CREATE_PROPERTY_ONFLOOR_TABLE);
        db.execSQL(CREATE_PROPERTY_SIZE_TABLE);
        db.execSQL(CREATE_PROPERTY_AVAILABILITY_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {

        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PROPERTY_TYPE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PROPERTY_SIZE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PROPERTY_REFFERED_BY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PROPERTY_AVAILABILITY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PROPERTY_AGE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PROPERTY_ONFLOOR);

        // Create tables again
        onCreate(db);
    }
}
