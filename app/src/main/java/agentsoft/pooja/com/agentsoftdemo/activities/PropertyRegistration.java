package agentsoft.pooja.com.agentsoftdemo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;


import agentsoft.pooja.com.agentsoftdemo.R;

public class PropertyRegistration extends AppCompatActivity {
    private Toolbar mToolbar;
    private LinearLayout ll_more;
    private Spinner sp_prop_type,sp_prop_size,sp_prop_age,sp_prop_onfloor,sp_prop_src;
    private ArrayAdapter<String> propTypeAdapter,propSizeAdapter,propAgeAdapter,propFloorAdapter,propSrcAdapter;
  // private MaterialSpinner sp_prop_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property_registration);
        setAppToolbar();
        attachViews();


    }

    private void attachViews() {
        ll_more=(LinearLayout)findViewById(R.id.ll_more);
        sp_prop_type=(Spinner)findViewById(R.id.sp_prop_type);
        sp_prop_size=(Spinner)findViewById(R.id.sp_prop_size);
        sp_prop_age=(Spinner)findViewById(R.id.sp_prop_age);
        sp_prop_onfloor=(Spinner)findViewById(R.id.sp_prop_onfloor);
        sp_prop_src=(Spinner)findViewById(R.id.sp_prop_src);


        propTypeAdapter = new ArrayAdapter<String>(this,
                R.layout.spinner_item, getResources().getStringArray(R.array.property_type));
        propTypeAdapter.setDropDownViewResource(R.layout.spinner_item);
        sp_prop_type.setAdapter(propTypeAdapter);

        propSizeAdapter = new ArrayAdapter<String>(this,
                R.layout.spinner_item, getResources().getStringArray(R.array.property_size));
        propSizeAdapter.setDropDownViewResource(R.layout.spinner_item);
        sp_prop_size.setAdapter(propSizeAdapter);

        propAgeAdapter = new ArrayAdapter<String>(this,
                R.layout.spinner_item, getResources().getStringArray(R.array.property_age));
        propAgeAdapter.setDropDownViewResource(R.layout.spinner_item);
        sp_prop_age.setAdapter(propAgeAdapter);

        propFloorAdapter = new ArrayAdapter<String>(this,
                R.layout.spinner_item, getResources().getStringArray(R.array.property_onfloor));
        propFloorAdapter.setDropDownViewResource(R.layout.spinner_item);
        sp_prop_onfloor.setAdapter(propFloorAdapter);

        propSrcAdapter = new ArrayAdapter<String>(this,
                R.layout.spinner_item, getResources().getStringArray(R.array.property_src));
        propSrcAdapter.setDropDownViewResource(R.layout.spinner_item);
        sp_prop_src.setAdapter(propSrcAdapter);
    }

    private void setAppToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(PropertyRegistration.this,MainActivity.class));
        finish();

    }
}
