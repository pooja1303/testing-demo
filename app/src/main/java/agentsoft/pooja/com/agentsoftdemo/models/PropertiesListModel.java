package agentsoft.pooja.com.agentsoftdemo.models;

import android.graphics.drawable.Drawable;

/**
 * Created by POOJA on 9/17/2017.
 */

public class PropertiesListModel {
    public Drawable property_img;
    public String property_for;
    public String property_type;
    public String locality;
    public String area;
    public String property_size;
    public String status;
    public String rent;
    public String deposit;
    public String property_availability;

    public PropertiesListModel(Drawable property_img, String property_for, String property_type, String locality, String area, String property_size, String status, String rent, String deposit, String property_availability) {
        this.property_img = property_img;
        this.property_for = property_for;
        this.property_type = property_type;
        this.locality = locality;
        this.area = area;
        this.property_size = property_size;
        this.status = status;
        this.rent = rent;
        this.deposit = deposit;
        this.property_availability = property_availability;
    }
}
