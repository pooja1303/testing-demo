package agentsoft.pooja.com.agentsoftdemo.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;

import agentsoft.pooja.com.agentsoftdemo.R;

public class SplashScreen extends AppCompatActivity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;
    private SharedPreferences mPref;
    private String loginValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);
        mPref = getSharedPreferences("Login", Context.MODE_PRIVATE);
        loginValue = mPref.getString("isLogged", "");
        Log.v("", "loginValue " + loginValue);
        setSplashtimeout();


    }

    private void setSplashtimeout() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (loginValue.equals("1")) {
                    Intent intent = new Intent(SplashScreen.this, LoginWithPasscode.class);
                    intent.putExtra("login", "1");
                    startActivity(intent);
                    finish();
                } else {
                    SharedPreferences.Editor editor = mPref.edit();
                    editor.putString("isLogged", "0");
                    editor.commit();

                    Intent i = new Intent(SplashScreen.this, LoginScreen.class);
                    i.putExtra("login","0");
                    startActivity(i);
                    finish();

                }


            }
        }, SPLASH_TIME_OUT);
    }


}
