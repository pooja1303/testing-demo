package agentsoft.pooja.com.agentsoftdemo.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

import java.util.ArrayList;

import agentsoft.pooja.com.agentsoftdemo.activities.CustomerRegistration;
import agentsoft.pooja.com.agentsoftdemo.activities.PropertyRegistration;
import agentsoft.pooja.com.agentsoftdemo.adapters.PropertiesAdapter;
import agentsoft.pooja.com.agentsoftdemo.R;
import agentsoft.pooja.com.agentsoftdemo.models.PropertiesListModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class PropertyListFragment extends Fragment {
    private Context mContext;
    private RecyclerView mPropRecycler;
    private LinearLayoutManager mLayoutManager;
    private PropertiesAdapter mAdapter;
    private ArrayList<PropertiesListModel> mPropList;



    public PropertyListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_property_list, container, false);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Properties");
        setHasOptionsMenu(true);
        mContext = getActivity();
        setUpRecyclerView(view);
        return view;
    }


    private void setUpRecyclerView(View view) {

        mPropRecycler = (RecyclerView) view.findViewById(R.id.property_recyclerView);
        mPropRecycler.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mPropRecycler.setLayoutManager(mLayoutManager);
        setRecyclerAdapter();
    }

    private void setRecyclerAdapter() {
        addPropertiesList();
        mAdapter = new PropertiesAdapter(mContext,mPropList);
        mPropRecycler.setAdapter(mAdapter);
    }

    private void addPropertiesList() {
        mPropList = new ArrayList<>();
        for (int i = 0; i <= 10; i++) {
            mPropList.add(new PropertiesListModel(getResources().getDrawable(R.mipmap.ic_launcher_round),
                    "Rent", "Flat for Any", "Kothrud", "kothrud", "1 BHK", "Confirmed", "11000", "12000", "Immediate"));

        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        if(id==R.id.action_add){
            Intent intent = new Intent(getContext(), PropertyRegistration.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
