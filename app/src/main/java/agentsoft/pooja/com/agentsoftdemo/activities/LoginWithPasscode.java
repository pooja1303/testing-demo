package agentsoft.pooja.com.agentsoftdemo.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import agentsoft.pooja.com.agentsoftdemo.R;

public class LoginWithPasscode extends AppCompatActivity {
    private SharedPreferences mPref;
    private TextView txtForgotPasscode;
    private EditText edtPasscode;
    private Button btnLogin;
    private String loginValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_with_passcode);

        getLoginPref();
        initializeComponents();
        setListeners();
    }

    private void setListeners() {
        txtForgotPasscode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginWithPasscode.this, LoginScreen.class);
                intent.putExtra("login","0");
                startActivity(intent);
                finish();
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (loginValue.equals("1")) {
                    Intent intent = new Intent(LoginWithPasscode.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    private void initializeComponents() {
        txtForgotPasscode = (TextView) findViewById(R.id.txt_forgot_passcode);
        edtPasscode = (EditText) findViewById(R.id.edt_passcode);
        btnLogin = (Button) findViewById(R.id.btnLogin);
    }

    private void getLoginPref() {
        mPref = getSharedPreferences("Login", Context.MODE_PRIVATE);
        loginValue = mPref.getString("isLogged", "");

    }
}
