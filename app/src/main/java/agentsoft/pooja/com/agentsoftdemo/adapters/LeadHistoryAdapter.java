package agentsoft.pooja.com.agentsoftdemo.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import agentsoft.pooja.com.agentsoftdemo.R;
import agentsoft.pooja.com.agentsoftdemo.models.LeadHistoryModel;

/**
 * Created by POOJA on 9/18/2017.
 */

public class LeadHistoryAdapter  extends RecyclerView.Adapter<LeadHistoryAdapter.ViewHolder> {

    Context mContext;
    ArrayList<LeadHistoryModel> mList;

    public LeadHistoryAdapter(Context mContext, ArrayList<LeadHistoryModel> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_lead_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder  extends RecyclerView.ViewHolder{
        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
