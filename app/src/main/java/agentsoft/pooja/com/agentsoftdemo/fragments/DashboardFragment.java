package agentsoft.pooja.com.agentsoftdemo.fragments;


import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.Arrays;

import agentsoft.pooja.com.agentsoftdemo.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardFragment extends Fragment implements OnChartValueSelectedListener {
    private PieChart mChart1, mChart2, mChart3;
    private SeekBar mSeekBarX, mSeekBarY;
    private Context mContext;
    private String[] mParties = new String[]{"Gone", "Confirmed", "Un Confirmed"};
    private String[] mParties2 = new String[]{"Gone", "Confirmed", "Not Registered"};
    private String[] mParties3 = new String[]{"Not Reachable", "Confirmed"};


    public DashboardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Dashboard");
        setHasOptionsMenu(false);
        mContext = getActivity();
        attachViews(view);
        return view;
    }

    private void attachViews(View view) {
        // mSeekBarX = (SeekBar) findViewById(R.id.seekBar1);
        // mSeekBarY = (SeekBar) findViewById(R.id.seekBar2);
        // mSeekBarY.setProgress(10);
        // mSeekBarX.setOnSeekBarChangeListener(MainActivity.this);


        mChart1 = (PieChart)view. findViewById(R.id.chart1);
        mChart2 = (PieChart)view. findViewById(R.id.chart2);
        mChart3 = (PieChart)view. findViewById(R.id.chart3);

        setValuesForChart1(mChart1);
        setValuesForChart2(mChart2);
        setValuesForChart3(mChart3);
    }
    private void setValuesForChart3(PieChart mChart3) {

        mChart3.setUsePercentValues(true);
        mChart3.getDescription().setEnabled(false);
        mChart3.setExtraOffsets(5, 10, 5, 5);
        mChart3.setDragDecelerationFrictionCoef(0.95f);

//        mChart1.setCenterTextTypeface(Typeface.createFromAsset(getAssets(), "OpenSans-Light.ttf"));
        // mChart1.setCenterText(generateCenterSpannableText());
        // mChart2.setCenterText(generateCenterSpannableText());
        //mChart3.setCenterText(generateCenterSpannableText());

        mChart3.setExtraOffsets(20.f, 0.f, 20.f, 0.f);
        mChart3.setDrawHoleEnabled(true);
        mChart3.setHoleColor(Color.WHITE);
        mChart3.setTransparentCircleColor(Color.WHITE);
        mChart3.setTransparentCircleAlpha(110);
        mChart3.setHoleRadius(58f);
        mChart3.setTransparentCircleRadius(61f);

        // mChart1.setDrawCenterText(true);
        // mChart2.setDrawCenterText(true);
        // mChart3.setDrawCenterText(true);

        mChart3.setRotationAngle(0);
        // enable rotation of the chart by touch
        mChart3.setRotationEnabled(true);
        mChart3.setHighlightPerTapEnabled(true);

        // mChart1.setUnit(" €");
        // mChart1.setDrawUnitsInChart(true);

        // add a selection listener
        mChart3.setOnChartValueSelectedListener(this);
        setData3(2, 100, mParties3);
        mChart3.animateY(1400, Easing.EasingOption.EaseInOutBack);
    }

    private void setValuesForChart2(PieChart mChart2) {
        mChart2.setUsePercentValues(true);
        mChart2.getDescription().setEnabled(false);
        mChart2.setExtraOffsets(5, 10, 5, 5);
        mChart2.setDragDecelerationFrictionCoef(0.95f);
        mChart2.setExtraOffsets(20.f, 0.f, 20.f, 0.f);
        mChart2.setDrawHoleEnabled(true);
        mChart2.setHoleColor(Color.WHITE);
        mChart2.setTransparentCircleColor(Color.WHITE);
        mChart2.setTransparentCircleAlpha(110);
        mChart2.setHoleRadius(58f);
        mChart2.setTransparentCircleRadius(61f);
        mChart2.setRotationAngle(0);
        mChart2.setRotationEnabled(true);
        mChart2.setHighlightPerTapEnabled(true);
        mChart2.setOnChartValueSelectedListener(this);
        mChart2.animateY(1400, Easing.EasingOption.EaseInOutCirc);
        setData2(3, 100, mParties2);
    }

    private void setValuesForChart1(PieChart mChart1) {
        mChart1.setUsePercentValues(true);
        mChart1.getDescription().setEnabled(false);
        mChart1.setExtraOffsets(5, 10, 5, 5);
        mChart1.setDragDecelerationFrictionCoef(0.95f);
        mChart1.setExtraOffsets(20.f, 0.f, 20.f, 0.f);
        mChart1.setDrawHoleEnabled(true);
        mChart1.setHoleColor(Color.WHITE);
        mChart1.setTransparentCircleColor(Color.WHITE);
        mChart1.setTransparentCircleAlpha(110);
        mChart1.setHoleRadius(58f);
        mChart1.setTransparentCircleRadius(61f);
        mChart1.setRotationAngle(0);
        mChart1.setRotationEnabled(true);
        mChart1.setHighlightPerTapEnabled(true);
        mChart1.setOnChartValueSelectedListener(this);
        mChart1.animateY(1400, Easing.EasingOption.EaseInOutQuad);

        setData(3, 100, mParties);
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        if (e == null)
            return;
    }

    @Override
    public void onNothingSelected() {

    }

    public void setData(int count, int range, String[] dataList) {
        Log.v("","count " +count +" dataList " + Arrays.toString(dataList));
        float mult = range;

        ArrayList<PieEntry> entries = new ArrayList<PieEntry>();

        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.
        for (int k = 0; k < count; k++) {
            entries.add(new PieEntry((float) (Math.random() * mult) + mult / 5, mParties[k % mParties.length]));
        }



        PieDataSet dataSet = new PieDataSet(entries, "");
        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(5f);

        ArrayList<Integer> colors = new ArrayList<Integer>();

       /* for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);*/

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);


        dataSet.setValueLinePart1OffsetPercentage(80.f);
        dataSet.setValueLinePart1Length(0.2f);
        dataSet.setValueLinePart2Length(0.4f);
        //dataSet.setXValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
        dataSet.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.BLACK);

        //data.setValueTypeface(tf);
        mChart1.setData(data);

        // undo all highlights
        mChart1.highlightValues(null);

        mChart1.invalidate();
    }
    public void setData2(int count, int range, String[] dataList) {
        Log.v("","count " +count +" dataList " + Arrays.toString(dataList));
        float mult = range;

        ArrayList<PieEntry> entries = new ArrayList<PieEntry>();

        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.

        for (int k = 0; k < count; k++) {
            entries.add(new PieEntry((float) (Math.random() * mult) + mult / 5, mParties2[k % mParties2.length]));
        }



        PieDataSet dataSet = new PieDataSet(entries, "");
        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(5f);

        ArrayList<Integer> colors = new ArrayList<Integer>();

       /* for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);*/

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);


        dataSet.setValueLinePart1OffsetPercentage(80.f);
        dataSet.setValueLinePart1Length(0.2f);
        dataSet.setValueLinePart2Length(0.4f);
        //dataSet.setXValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
        dataSet.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.BLACK);

        //data.setValueTypeface(tf);
        mChart2.setData(data);

        // undo all highlights
        mChart2.highlightValues(null);

        mChart2.invalidate();
    }
    public void setData3(int count, int range, String[] dataList) {
        Log.v("","count " +count +" dataList " + Arrays.toString(dataList));
        float mult = range;

        ArrayList<PieEntry> entries = new ArrayList<PieEntry>();

        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.

        for (int k = 0; k < count; k++) {
            entries.add(new PieEntry((float) (Math.random() * mult) + mult / 5, mParties3[k % mParties3.length]));
        }


        PieDataSet dataSet = new PieDataSet(entries, "");
        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(5f);

        ArrayList<Integer> colors = new ArrayList<Integer>();

       /* for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);*/

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);


        dataSet.setValueLinePart1OffsetPercentage(80.f);
        dataSet.setValueLinePart1Length(0.2f);
        dataSet.setValueLinePart2Length(0.4f);
        //dataSet.setXValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
        dataSet.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.BLACK);

        //data.setValueTypeface(tf);
        mChart3.setData(data);

        // undo all highlights
        mChart3.highlightValues(null);

        mChart3.invalidate();
    }
}
