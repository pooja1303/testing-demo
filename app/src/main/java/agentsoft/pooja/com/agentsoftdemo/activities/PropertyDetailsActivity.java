package agentsoft.pooja.com.agentsoftdemo.activities;

import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import agentsoft.pooja.com.agentsoftdemo.R;
import agentsoft.pooja.com.agentsoftdemo.fragments.PropertyListFragment;

public class PropertyDetailsActivity extends AppCompatActivity {
    private Toolbar mToolbar;
    private  String from;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property_details);
        setAppToolbar();
        attachViews();
    }

    private void attachViews() {
         from=getIntent().getExtras().getString("from");
    }

    private void setAppToolbar() {

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
      //  if(from.equals("property")) {
          //  startActivity(new Intent(PropertyDetailsActivity.this, MainActivity.class));
           // finish();
//            FragmentTransaction propFrag = getSupportFragmentManager().beginTransaction();
//            propFrag.replace(R.id.content_frame, new PropertyListFragment());
//            propFrag.commit();
            finish();
        //}

    }
}
