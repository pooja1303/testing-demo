package agentsoft.pooja.com.agentsoftdemo.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import agentsoft.pooja.com.agentsoftdemo.activities.CustomerRegistration;
import agentsoft.pooja.com.agentsoftdemo.activities.MainActivity;
import agentsoft.pooja.com.agentsoftdemo.adapters.CustomerAdapter;
import agentsoft.pooja.com.agentsoftdemo.R;
import agentsoft.pooja.com.agentsoftdemo.models.CustomerListModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class CustomerListFragment extends Fragment {
    private Context mContext;
    private RecyclerView mCustRecycler;
    private LinearLayoutManager mLayoutManager;
    private CustomerAdapter mAdapter;
    private ArrayList<CustomerListModel> mCustList;
    private Toolbar toolbar;


    public CustomerListFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_customer_list, container, false);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Customer's");
        setHasOptionsMenu(true);
        mContext = getActivity();
        //toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        //((AppCompatActivity) getActivity()).getSupportActionBar(toolbar);
        settingUpRecyclerView(view);
        return view;
    }

    private void settingUpRecyclerView(View view) {
        mCustRecycler = (RecyclerView) view.findViewById(R.id.customer_recyclerView);
        mCustRecycler.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mCustRecycler.setLayoutManager(mLayoutManager);
        setRecyclerAdapter();
    }

    private void setRecyclerAdapter() {
        addCustomerList();
        mAdapter = new CustomerAdapter(mContext,mCustList);
        mCustRecycler.setAdapter(mAdapter);

    }

    private void addCustomerList() {
        mCustList = new ArrayList<>();
        for(int i=0;i<=10;i++) {
            mCustList.add(new CustomerListModel(getResources().getDrawable(R.drawable.empty_user),
                    "Ajith Pawar","9890000000","Rent","kothrud","1 BHK","Confirmed","11000 - 12000","Immediate"));
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        if(id==R.id.action_add){
            Intent intent = new Intent(getContext(), CustomerRegistration.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
