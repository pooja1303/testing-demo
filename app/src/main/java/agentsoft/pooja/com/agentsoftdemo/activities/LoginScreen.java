package agentsoft.pooja.com.agentsoftdemo.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import agentsoft.pooja.com.agentsoftdemo.Db.DatabaseHelper;
import agentsoft.pooja.com.agentsoftdemo.R;

/**
 * Created by POOJA on 8/26/2017.
 */

public class LoginScreen extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 100;

    private EditText edtUserName, edtPassword, edtPasscode;
    private Button btnLogin;
    private String username, password, passcode, imeiNo;
    private TelephonyManager telMgr;
    private ProgressDialog pDialog;
    private SharedPreferences mPref;
    private String value = "0";
    private DatabaseHelper db;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_screen);

        //db = new DatabaseHelper(getApplicationContext());

        setRuntimePermissions();
        intializeComponents();
        // getDeviceImeiNo();
        setListeners();
        //setBlurryBackground();


    }

    private void setRuntimePermissions() {
        if (Build.VERSION.SDK_INT < 23) {
            getDeviceImeiNo();
        } else {
            getDevicePermissions();
        }
    }

    private void getDevicePermissions() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            // Camera permission has not been granted.

            requestPermission();

        } else {

            // Camera permissions is already available, show the camera preview.
            getDeviceImeiNo();
        }
    }

    private void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.READ_PHONE_STATE)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
            new AlertDialog.Builder(LoginScreen.this)
                    .setTitle("Permission Request")
                    .setMessage(getString(R.string.permission_read_phone_state_rationale))
                    .setCancelable(false)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            //re-request
                            ActivityCompat.requestPermissions(LoginScreen.this,
                                    new String[]{Manifest.permission.READ_PHONE_STATE},
                                    MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);
                        }
                    })
                    .show();
        } else {
            // READ_PHONE_STATE permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE},
                    MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);
        }
    }

    private void getDeviceImeiNo() {
        telMgr = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        imeiNo = telMgr.getDeviceId();
    }

    private void setBlurryBackground() {
       /* Blurry.with(LoginScreen.this)
                .radius(50)
                .sampling(2)
                .async()
                .color(getResources().getColor(R.color.md_grey_200))
                .onto((ViewGroup) findViewById(R.id.parentLayout));*/

        // Blurry.with(LoginScreen.this).radius(25).sampling(2).onto((LinearLayout) findViewById(R.id.parentLayout));
    }

    private void setListeners() {
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                username = edtUserName.getText().toString();
                password = edtPassword.getText().toString();
                passcode = edtPasscode.getText().toString(); // passcode+imei no

                if (!username.isEmpty() && !password.isEmpty() && !passcode.isEmpty()) {
                    //callLoginService();
                    String loginVal = getIntent().getStringExtra("login");
                    if (loginVal.equals("0")) {

                        SharedPreferences.Editor editor = mPref.edit();
                        editor.putString("isLogged", "1");
                        editor.commit();

                        Intent intent = new Intent(LoginScreen.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), "Please enter correct credentials.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void callLoginService() {
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Logging in ...");
        pDialog.show();

      /*  StringRequest strReq = new StringRequest(Request.Method.POST, Services.LoginWebService.URL_LOGIN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });*/
    }

    private void intializeComponents() {
        edtUserName = (EditText) findViewById(R.id.edt_username);
        edtPasscode = (EditText) findViewById(R.id.edt_passcode);
        edtPassword = (EditText) findViewById(R.id.edt_password);

        btnLogin = (Button) findViewById(R.id.btnLogin);

        mPref = getSharedPreferences("Login", Context.MODE_PRIVATE);


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //  super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSIONS_REQUEST_READ_PHONE_STATE) {
            // Received permission result for READ_PHONE_STATE permission.est.");
            // Check if the only required permission has been granted
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // READ_PHONE_STATE permission has been granted, proceed with displaying IMEI Number
                //alertAlert(getString(R.string.permision_available_read_phone_state));
                getDeviceImeiNo();
            } else {
                alertAlert(getString(R.string.permissions_not_granted_read_phone_state));
            }
        }
    }

    private void alertAlert(String msg) {
        new AlertDialog.Builder(LoginScreen.this)
                .setTitle("Permission Request")
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do somthing here
                    }
                })
                .show();
    }


}

/*


 String login = mPref.getString("isLogged", value);
                    if (login.equals("0")) {
                        //Open the login activity and set this so that next it value is 1 then this conditin will be false.
                        SharedPreferences.Editor editor = mPref.edit();
                        editor.putString("isLogged", "1");
                        editor.commit();
                        Intent intent = new Intent(LoginScreen.this, MainActivity.class);
                   /* intent.putExtra("login", "1");
                    setResult(RESULT_OK, intent);*/
//startActivity(intent);
//  finish();

/*} else {
                        //Open this Home activity
                        Intent intent = new Intent(LoginScreen.this, LoginWithPasscode.class);
                        startActivity(intent);
                        finish();
                    }*/
                    /*SharedPreferences.Editor editor = mPref.edit();
                    editor.putString("isLogged", "1");
                    editor.commit();

                    Intent intent = new Intent(LoginScreen.this, LoginWithPasscode.class);
                    startActivity(intent);
                    finish();


                    */


